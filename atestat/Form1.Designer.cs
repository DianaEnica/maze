﻿namespace atestat
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.screen = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.c5 = new System.Windows.Forms.Label();
            this.c4 = new System.Windows.Forms.Label();
            this.c3 = new System.Windows.Forms.Label();
            this.c2 = new System.Windows.Forms.Label();
            this.c1 = new System.Windows.Forms.Label();
            this.player = new System.Windows.Forms.PictureBox();
            this.finish = new System.Windows.Forms.Label();
            this.b6 = new System.Windows.Forms.Label();
            this.b14 = new System.Windows.Forms.Label();
            this.b7 = new System.Windows.Forms.Label();
            this.b16 = new System.Windows.Forms.Label();
            this.b15 = new System.Windows.Forms.Label();
            this.b5 = new System.Windows.Forms.Label();
            this.b11 = new System.Windows.Forms.Label();
            this.b12 = new System.Windows.Forms.Label();
            this.b13 = new System.Windows.Forms.Label();
            this.b10 = new System.Windows.Forms.Label();
            this.b8 = new System.Windows.Forms.Label();
            this.b9 = new System.Windows.Forms.Label();
            this.b3 = new System.Windows.Forms.Label();
            this.b2 = new System.Windows.Forms.Label();
            this.b4 = new System.Windows.Forms.Label();
            this.b1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.screen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.player)).BeginInit();
            this.SuspendLayout();
            // 
            // screen
            // 
            this.screen.BackColor = System.Drawing.SystemColors.Desktop;
            this.screen.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("screen.BackgroundImage")));
            this.screen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.screen.Controls.Add(this.label1);
            this.screen.Controls.Add(this.c5);
            this.screen.Controls.Add(this.c4);
            this.screen.Controls.Add(this.c3);
            this.screen.Controls.Add(this.c2);
            this.screen.Controls.Add(this.c1);
            this.screen.Controls.Add(this.player);
            this.screen.Controls.Add(this.finish);
            this.screen.Controls.Add(this.b6);
            this.screen.Controls.Add(this.b14);
            this.screen.Controls.Add(this.b7);
            this.screen.Controls.Add(this.b16);
            this.screen.Controls.Add(this.b15);
            this.screen.Controls.Add(this.b5);
            this.screen.Controls.Add(this.b11);
            this.screen.Controls.Add(this.b12);
            this.screen.Controls.Add(this.b13);
            this.screen.Controls.Add(this.b10);
            this.screen.Controls.Add(this.b8);
            this.screen.Controls.Add(this.b9);
            this.screen.Controls.Add(this.b3);
            this.screen.Controls.Add(this.b2);
            this.screen.Controls.Add(this.b4);
            this.screen.Controls.Add(this.b1);
            this.screen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.screen.Location = new System.Drawing.Point(0, 0);
            this.screen.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.screen.Name = "screen";
            this.screen.Size = new System.Drawing.Size(752, 442);
            this.screen.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Turquoise;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(623, 212);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 20);
            this.label1.TabIndex = 24;
            this.label1.Text = "Total=0";
            // 
            // c5
            // 
            this.c5.BackColor = System.Drawing.Color.Yellow;
            this.c5.ForeColor = System.Drawing.Color.Black;
            this.c5.Location = new System.Drawing.Point(323, 239);
            this.c5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.c5.Name = "c5";
            this.c5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.c5.Size = new System.Drawing.Size(27, 25);
            this.c5.TabIndex = 23;
            // 
            // c4
            // 
            this.c4.BackColor = System.Drawing.Color.Lime;
            this.c4.ForeColor = System.Drawing.Color.Black;
            this.c4.Location = new System.Drawing.Point(312, 229);
            this.c4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.c4.Name = "c4";
            this.c4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.c4.Size = new System.Drawing.Size(27, 25);
            this.c4.TabIndex = 22;
            // 
            // c3
            // 
            this.c3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.c3.ForeColor = System.Drawing.Color.Black;
            this.c3.Location = new System.Drawing.Point(319, 303);
            this.c3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.c3.Name = "c3";
            this.c3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.c3.Size = new System.Drawing.Size(27, 25);
            this.c3.TabIndex = 21;
            // 
            // c2
            // 
            this.c2.BackColor = System.Drawing.SystemColors.Highlight;
            this.c2.ForeColor = System.Drawing.Color.Black;
            this.c2.Location = new System.Drawing.Point(319, 175);
            this.c2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.c2.Name = "c2";
            this.c2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.c2.Size = new System.Drawing.Size(27, 25);
            this.c2.TabIndex = 20;
            // 
            // c1
            // 
            this.c1.BackColor = System.Drawing.Color.Fuchsia;
            this.c1.ForeColor = System.Drawing.Color.Black;
            this.c1.Location = new System.Drawing.Point(473, 187);
            this.c1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.c1.Name = "c1";
            this.c1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.c1.Size = new System.Drawing.Size(27, 25);
            this.c1.TabIndex = 19;
            // 
            // player
            // 
            this.player.BackColor = System.Drawing.Color.Red;
            this.player.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.player.Location = new System.Drawing.Point(48, 79);
            this.player.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.player.Name = "player";
            this.player.Size = new System.Drawing.Size(19, 18);
            this.player.TabIndex = 0;
            this.player.TabStop = false;
            // 
            // finish
            // 
            this.finish.AutoSize = true;
            this.finish.BackColor = System.Drawing.SystemColors.Desktop;
            this.finish.CausesValidation = false;
            this.finish.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.finish.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.finish.Location = new System.Drawing.Point(509, 378);
            this.finish.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.finish.Name = "finish";
            this.finish.Size = new System.Drawing.Size(57, 17);
            this.finish.TabIndex = 18;
            this.finish.Text = "FINISH";
            this.finish.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // b6
            // 
            this.b6.BackColor = System.Drawing.Color.DarkTurquoise;
            this.b6.CausesValidation = false;
            this.b6.Location = new System.Drawing.Point(480, 105);
            this.b6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.b6.Name = "b6";
            this.b6.Size = new System.Drawing.Size(113, 15);
            this.b6.TabIndex = 17;
            // 
            // b14
            // 
            this.b14.BackColor = System.Drawing.Color.DarkTurquoise;
            this.b14.CausesValidation = false;
            this.b14.Location = new System.Drawing.Point(476, 166);
            this.b14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.b14.Name = "b14";
            this.b14.Size = new System.Drawing.Size(13, 267);
            this.b14.TabIndex = 16;
            // 
            // b7
            // 
            this.b7.BackColor = System.Drawing.Color.DarkTurquoise;
            this.b7.CausesValidation = false;
            this.b7.Location = new System.Drawing.Point(400, 47);
            this.b7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.b7.Name = "b7";
            this.b7.Size = new System.Drawing.Size(13, 331);
            this.b7.TabIndex = 15;
            // 
            // b16
            // 
            this.b16.BackColor = System.Drawing.Color.DarkTurquoise;
            this.b16.CausesValidation = false;
            this.b16.Location = new System.Drawing.Point(83, 366);
            this.b16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.b16.Name = "b16";
            this.b16.Size = new System.Drawing.Size(257, 12);
            this.b16.TabIndex = 14;
            // 
            // b15
            // 
            this.b15.BackColor = System.Drawing.Color.DarkTurquoise;
            this.b15.CausesValidation = false;
            this.b15.Location = new System.Drawing.Point(15, 303);
            this.b15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.b15.Name = "b15";
            this.b15.Size = new System.Drawing.Size(263, 12);
            this.b15.TabIndex = 13;
            // 
            // b5
            // 
            this.b5.BackColor = System.Drawing.Color.DarkTurquoise;
            this.b5.CausesValidation = false;
            this.b5.Location = new System.Drawing.Point(101, 105);
            this.b5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.b5.Name = "b5";
            this.b5.Size = new System.Drawing.Size(177, 12);
            this.b5.TabIndex = 12;
            // 
            // b11
            // 
            this.b11.BackColor = System.Drawing.Color.DarkTurquoise;
            this.b11.CausesValidation = false;
            this.b11.Location = new System.Drawing.Point(83, 235);
            this.b11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.b11.Name = "b11";
            this.b11.Size = new System.Drawing.Size(252, 12);
            this.b11.TabIndex = 11;
            // 
            // b12
            // 
            this.b12.BackColor = System.Drawing.Color.DarkTurquoise;
            this.b12.CausesValidation = false;
            this.b12.Location = new System.Drawing.Point(83, 160);
            this.b12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.b12.Name = "b12";
            this.b12.Size = new System.Drawing.Size(13, 87);
            this.b12.TabIndex = 10;
            // 
            // b13
            // 
            this.b13.BackColor = System.Drawing.Color.DarkTurquoise;
            this.b13.CausesValidation = false;
            this.b13.Location = new System.Drawing.Point(211, 161);
            this.b13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.b13.Name = "b13";
            this.b13.Size = new System.Drawing.Size(13, 86);
            this.b13.TabIndex = 9;
            // 
            // b10
            // 
            this.b10.BackColor = System.Drawing.Color.DarkTurquoise;
            this.b10.CausesValidation = false;
            this.b10.Location = new System.Drawing.Point(145, 106);
            this.b10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.b10.Name = "b10";
            this.b10.Size = new System.Drawing.Size(13, 82);
            this.b10.TabIndex = 8;
            // 
            // b8
            // 
            this.b8.BackColor = System.Drawing.Color.DarkTurquoise;
            this.b8.CausesValidation = false;
            this.b8.Location = new System.Drawing.Point(328, 105);
            this.b8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.b8.Name = "b8";
            this.b8.Size = new System.Drawing.Size(13, 273);
            this.b8.TabIndex = 7;
            // 
            // b9
            // 
            this.b9.BackColor = System.Drawing.Color.DarkTurquoise;
            this.b9.CausesValidation = false;
            this.b9.Location = new System.Drawing.Point(265, 106);
            this.b9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.b9.Name = "b9";
            this.b9.Size = new System.Drawing.Size(13, 87);
            this.b9.TabIndex = 6;
            // 
            // b3
            // 
            this.b3.BackColor = System.Drawing.Color.DarkTurquoise;
            this.b3.CausesValidation = false;
            this.b3.Location = new System.Drawing.Point(581, 46);
            this.b3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.b3.Name = "b3";
            this.b3.Size = new System.Drawing.Size(13, 388);
            this.b3.TabIndex = 5;
            // 
            // b2
            // 
            this.b2.BackColor = System.Drawing.Color.DarkTurquoise;
            this.b2.CausesValidation = false;
            this.b2.Location = new System.Drawing.Point(15, 46);
            this.b2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.b2.Name = "b2";
            this.b2.Size = new System.Drawing.Size(13, 388);
            this.b2.TabIndex = 4;
            // 
            // b4
            // 
            this.b4.BackColor = System.Drawing.Color.DarkTurquoise;
            this.b4.CausesValidation = false;
            this.b4.Location = new System.Drawing.Point(15, 418);
            this.b4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.b4.Name = "b4";
            this.b4.Size = new System.Drawing.Size(580, 18);
            this.b4.TabIndex = 3;
            // 
            // b1
            // 
            this.b1.BackColor = System.Drawing.Color.DarkTurquoise;
            this.b1.CausesValidation = false;
            this.b1.Location = new System.Drawing.Point(15, 43);
            this.b1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.b1.Name = "b1";
            this.b1.Size = new System.Drawing.Size(580, 18);
            this.b1.TabIndex = 1;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(752, 442);
            this.Controls.Add(this.screen);
            this.ForeColor = System.Drawing.Color.Transparent;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "MyGame";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            this.screen.ResumeLayout(false);
            this.screen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.player)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel screen;
        private System.Windows.Forms.PictureBox player;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label b13;
        private System.Windows.Forms.Label b10;
        private System.Windows.Forms.Label b8;
        private System.Windows.Forms.Label b9;
        private System.Windows.Forms.Label b3;
        private System.Windows.Forms.Label b2;
        private System.Windows.Forms.Label b4;
        private System.Windows.Forms.Label b1;
        private System.Windows.Forms.Label finish;
        private System.Windows.Forms.Label b6;
        private System.Windows.Forms.Label b14;
        private System.Windows.Forms.Label b7;
        private System.Windows.Forms.Label b16;
        private System.Windows.Forms.Label b15;
        private System.Windows.Forms.Label b5;
        private System.Windows.Forms.Label b11;
        private System.Windows.Forms.Label b12;
        private System.Windows.Forms.Label c1;
        private System.Windows.Forms.Label c5;
        private System.Windows.Forms.Label c4;
        private System.Windows.Forms.Label c3;
        private System.Windows.Forms.Label c2;
        private System.Windows.Forms.Label label1;

    }
}

